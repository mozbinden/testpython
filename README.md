# Example Static Website incorporating Pandas and Bokeh

Finally got something working in Jupyter, and don't want to keep loading and re-running it? Gitlab pages is a quick way to host HTML, and a lot of Python Data components are HTML-friendly. This is an example to show how straightforward it can be.

[See what it looks like here.](http://teebr.gitlab.io/python-data-pages/data.html)

---

## How it works

1. The `.gitlab-ci.yaml` calls `main.py`
2. `main.py` creates some data (this could be a query or something more useful) and a Bokeh plot.
3. The `pandas.DataFrame` and Bokeh plot are rendered into HTML, and inserted into the Jinja template in `templates`. This creates the HTML file in `public`, which is the directory that Gitlab looks in to host.


